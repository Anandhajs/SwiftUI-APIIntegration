//
//  SwiftUI_APIIntegrationApp.swift
//  SwiftUI-APIIntegration
//
//  Created by Admin on 21/01/23.
//

import SwiftUI

@main
struct SwiftUI_APIIntegrationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
