//
//  LoginResponseModel.swift
//  SwiftUI-APIIntegration
//
//  Created by Admin on 02/02/23.
//

import Foundation

struct LoginResponseModel: Codable {
  var token: String
}
