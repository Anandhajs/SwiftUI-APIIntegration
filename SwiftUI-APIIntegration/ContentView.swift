//
//  ContentView.swift
//  SwiftUI-APIIntegration
//
//  Created by Admin on 21/01/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var loginViewModel = LoginViewModel()
    var body: some View {
        HStack {
            TextField("Email", text: $loginViewModel.emailField)
                .font(Font.system(size: 15))
                .padding()
            Image(systemName: "person").foregroundColor(Color.green)
                .padding(.all)
            
        }
        
        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
        .padding()
        //  Divider()
        
        HStack {
            SecureField("Password", text: $loginViewModel.passwordField)
                .font(Font.system(size: 15))
                .padding()
            Image(systemName: "eye").foregroundColor(Color.green)
                .padding()
        }
        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
        .padding()
        
        Button(action: {let createLoginRequest = LoginRequestModel(email: loginViewModel.emailField, password: loginViewModel.passwordField)
                loginViewModel.createLogin(request: createLoginRequest)}, label: {
                    
                    Text("Login")
                        .bold()
                        .frame(width: 150, height: 40, alignment: .center)
                        .padding(.all)
                        .font(Font.system(size: 20))
                        .foregroundColor(.white)
                        .background(Color.green.cornerRadius(10))
                })
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
