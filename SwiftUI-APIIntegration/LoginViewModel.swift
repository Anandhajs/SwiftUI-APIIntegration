//
//  LoginViewModel.swift
//  SwiftUI-APIIntegration
//
//  Created by Admin on 02/02/23.
//

import Foundation
import Alamofire

class LoginViewModel: ObservableObject {
  @Published var emailField: String = ""
  @Published var passwordField: String = ""
  @Published var createLoginResponse: LoginResponseModel?
func createLogin(request: LoginRequestModel) {
    let params = ["email": request.email,
                  "password": request.password]
    APIServices.shared.callCreateLogin(parameters: params as Parameters, success: { response in
        if let response = response {
          print(response)
        }
    }, failure: { error in
        print(error)
    })
  }
}
