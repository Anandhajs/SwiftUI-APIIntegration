//
//  APIService.swift
//  SwiftUI-APIIntegration
//
//  Created by Admin on 31/01/23.
//

import Foundation
import Alamofire

struct APIServices {
  public static let shared = APIServices()
  func callCreateLogin(parameters: Parameters? = nil, success: @escaping (_ result: LoginResponseModel?) -> Void, failure: @escaping (_ failureMsg: FailureMessage) -> Void) {
    var headers = HTTPHeaders()
    headers["content-type"] = "application/json"
    APIManager.shared.callAPI(serverURL: "https://reqres.in/api/login", method: .post, headers: headers, parameters: parameters, success: { response in
      do {
        if let data = response.data {
          let createLoginResponse = try JSONDecoder().decode(LoginResponseModel.self, from: data)
          success(createLoginResponse)
        }
      } catch {
        failure(FailureMessage(error.localizedDescription))
      }
    }, failure: { error in
      failure(FailureMessage(error))
    })
  }
}
